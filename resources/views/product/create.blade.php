@extends('layouts.app')

@section('title')
New Product
@endsection

@section('content')
<div class="container p-5" style="background-color: white">
    <form action="/product" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <h3 class="mb-4">Create New Product</h3>
            <label for="name">Product Name</label>
            <input type="text" class="form-control" name="name" id="name" placeholder="Masukkan Nama Product">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="quantity">Quantity</label>
            <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Masukkan Jumlah Product">
            @error('quantity')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="price">Price</label>
            <input type="text" class="form-control" name="price" id="price" placeholder="Masukkan Harga Product">
            @error('price')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary m-2">Create</button>
    </form>
</div>
@endsection

