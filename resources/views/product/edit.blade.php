@extends('layouts.app')

@section('title')
Edit Product
@endsection

@section('content')
<div class="container p-5" style="background-color: white">
    <form action="/product/{{$product->id}}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="form-group">
            <h3 class="mb-4">Edit Product</h3>
            <label for="name">Product Name</label>
            <input type="text" class="form-control" name="name" id="name" value="{{$product->name}}" placeholder="Masukkan Nama Product">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="quantity">Quantity</label>
            <input type="text" class="form-control" name="quantity" id="quantity" value="{{$product->quantity}}" placeholder="Masukkan Jumlah Product">
            @error('quantity')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="price">Price</label>
            <input type="text" class="form-control" name="price" id="price" value="{{$product->price}}" placeholder="Masukkan Harga Product">
            @error('price')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary m-2">Update</button>
    </form>
</div>
@endsection

